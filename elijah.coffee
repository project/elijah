$ = jQuery

$ ->

  Drupal.behaviors.elijahContentToggle = ->
    attach: (context, settings) ->
      $('.toggle-me', context).bind 'click', ->
        coordinates = $(this).offset()
        coordinates.top = 35
        coordinates.left = coordinates.left - 42
        $("#block-menu-menu-content-types").toggle()
        $("#block-menu-menu-content-types").offset(coordinates)

  $("#sidebar-second .associated-person").live 'mouseover', ->
    $('.remove-referenced-person', @).show()

  $("#sidebar-second .associated-person").live 'mouseout', ->
    $('.remove-referenced-person', @).hide()
