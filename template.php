<?php

function elijah_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];

  if (empty($breadcrumb)) {
    $breadcrumb = array(l(t('home'), '<front>'));
  }

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' » ', $breadcrumb) . '</div>';
    return $output;
  }
}

/*
 * Implementation of hook_page_alter().
 */
function elijah_preprocess_page(&$vars) {
  $vars['content_menu'] = '<nav id="content-menu">' . _content_menu() . '</nav>';
}

function _content_menu() {
  $result = db_query("SELECT DISTINCT type FROM {node}");
  $types = $result->fetchAll();
  $map = array(
    'autobiography' => array(
      'url' => 'autobiographies',
      'title' => 'Autobiographies'),
    'biography' => array(
      'url' => 'biographies',
      'title' => 'Biographies'),
    'funeral' => array(
      'url' => 'funerals',
      'title' => 'Funerals'),
    'gravesite' => array(
      'url' => 'gravesites',
      'title' => 'Gravesites'),
    'journal' => array(
      'url' => 'journals',
      'title' => 'Journals'),
    'letter' => array(
      'url' => 'letters',
      'title' => 'Letters'),
    'obituary' => array(
      'url' => 'obituaries',
      'title' => 'Obituaries'),
    'patriarchal_blessing' => '', # We'll not give patriarchal blessings a regular menu for now.
    'story' => array(
      'url' => 'stories',
      'title' => 'Stories'),
    'will' => array(
      'url' => 'wills',
      'title' => 'Wills'),
  );
  $menu = array();
  foreach ($types as $type) {
    $type = $type->type;
    if (!empty($map[$type])) {
      $menu[] = l($map[$type]['title'], $map[$type]['url']);
    }
  }

  return theme('item_list', array('items' => $menu));
}
