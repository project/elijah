(function() {
  var $;
  $ = jQuery;
  $(function() {
    Drupal.behaviors.elijahContentToggle = function() {
      return {
        attach: function(context, settings) {
          return $('.toggle-me', context).bind('click', function() {
            var coordinates;
            coordinates = $(this).offset();
            coordinates.top = 35;
            coordinates.left = coordinates.left - 42;
            $("#block-menu-menu-content-types").toggle();
            return $("#block-menu-menu-content-types").offset(coordinates);
          });
        }
      };
    };
    $("#sidebar-second .associated-person").live('mouseover', function() {
      return $('.remove-referenced-person', this).show();
    });
    return $("#sidebar-second .associated-person").live('mouseout', function() {
      return $('.remove-referenced-person', this).hide();
    });
  });
}).call(this);
