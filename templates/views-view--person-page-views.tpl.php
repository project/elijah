<?php
/**
 * @file views-view.tpl.php
 * Main view template
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 * - $admin_links: A rendered list of administrative links
 * - $admin_links_raw: A list of administrative links suitable for theme('links')
 *
 * @ingroup views_templates
 */
// Generate event data.
$rows = array();
$birthdate = adodb_date('d M Y', $view->result[0]->fhd_person_birth_date);
$deathdate = adodb_date('d M Y', $view->result[0]->fhd_person_death_date);
$burialdate = adodb_date('d M Y', $view->result[0]->fhd_person_burial_date);
if (!empty($view->result[0]->fhd_person_birth_place)) {
  $rows[] = array(array('data' => 'Birth', 'class' => 'event-type'), $birthdate . ' ' . t('at') . ' ' . $view->result[0]->fhd_person_birth_place);
}
if (!empty($view->result[0]->fhd_person_death_place)) {
  $rows[] = array(array('data' => 'Death', 'class' => 'event-type'), $deathdate . ' ' . t('at') . ' ' . $view->result[0]->fhd_person_death_place);
}
if (!empty($view->result[0]->fhd_person_burial_place)) {
  $rows[] = array(array('data' => 'Burial', 'class' => 'event-type'), $deathdate . ' ' . t('at') . ' ' . $view->result[0]->fhd_person_burial_place);
}
?>
<div class="<?php print $classes; ?>">
  <h2>
    <?php print t('Events'); ?>
  </h2>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print theme('table', array('rows' => $rows)); ?>
    </div>
  <?php elseif (!empty($empty)): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

</div> <?php /* class view */ ?>
